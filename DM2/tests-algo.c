#include <stdio.h>
#include <stdlib.h>
#include <time.h>

void swap(int* a, int* b) {
    int t = *a;
    *a = *b;
    *b = t;
}

int partition(int arr[], int t[], int low, int high) {
    int pivot = t[arr[high]];
    int j;
    int i = (low - 1);

    for (j = low; j <= high - 1; j++) {
        if (t[arr[j]] <= pivot) {
            i++;
            swap(&arr[i], &arr[j]);
        }
    }

    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quickSort(int arr[], int t[], int low, int high) {
    if (low < high) {
        int pi = partition(arr, t, low, high);
        quickSort(arr, t, low, pi - 1);
        quickSort(arr, t, pi + 1, high);
    }
}

char* in_array(int val, int n, int* A, int* B) {
    int i;

    for (i = 0; i < n; i++) {
        if (A[i] == val)
            return "M1";
        if (B[i] == val)
            return "M2";
    }

    return "M0";
}

/* n    : nombre total de tâches à effectuer sur les machines M1 et M2
   t1   : tableau de n entiers où t1[i-1] est la durée d'exécution de la tâche i sur M1
   t2   : tableau de n entiers où t2[i-1] est la durée d'exécution de la tâche i sur M2
   time : un pointeur vers un entier (-1 au départ), contiendra la durée minimale
          d'exécution des tâches sur les deux machines */
void johnson_min_time_algorithm(int n, int* t1, int* t2, int* time) {
    int i, index_a, index_b;
    int* A = calloc(n, sizeof(int));
    int* B = calloc(n, sizeof(int));
    int* AB_concat = calloc(n, sizeof(int));
    int tmp;

    index_a = 0;
    index_b = 0;

    for (i = 0; i < n; i++) {
        if (t1[i] <= t2[i]) {
            A[index_a] = i;
            index_a += 1;
        } else {
            B[index_b] = i;
            index_b += 1;
        }
    }

    /* On trie A et B */
    quickSort(A, t1, 0, index_a - 1);
    quickSort(B, t2, 0, index_b - 1);

    /* On inverse le tri pour B */
    int end = index_b - 1;
    for (i = 0; i < index_b/2; i++) {
        tmp = B[i];
        B[i] = B[end];
        B[end] = tmp;
        end--;
    }

    /* On réalise la concaténation des deux ensembles */
    for (i = 0; i < index_a; i++)
        AB_concat[i] = A[i];
    for (i = 0; i < index_b; i++)
        AB_concat[i + index_a] = B[i];

    /* -------------- DEBUG ONLY -------------- */
    printf(" ==================================\n");
    printf(" |       M1 |       M2 |    Choix |\n");
    printf(" ==================================\n");
    for (i = 0; i < n; i++)
        printf(" | %8d | %8d | %8s |\n", t1[i], t2[i], in_array(i, n, A, B));
    printf(" ==================================\n");
    /* -------------- DEBUG ONLY -------------- */

    int curr_fin_M1 = 0;
    int curr_fin_M2 = t1[AB_concat[0]];
    for (i = 0; i < n; i++) {
        curr_fin_M1 = curr_fin_M1 + t1[AB_concat[i]];
        curr_fin_M2 = (curr_fin_M1 >= curr_fin_M2) ? curr_fin_M1 : curr_fin_M2;
        curr_fin_M2 = curr_fin_M2 + t2[AB_concat[i]];
    }

    *time = (curr_fin_M1 > curr_fin_M2) ? curr_fin_M1 : curr_fin_M2;
}

int main() {
    int n;
    clock_t begin, end;
    int time_johnson = -1;

    /* Génération aléatoire */

    n = 15;
    int i;
    int* t1 = calloc(n, sizeof(int));
    int* t2 = calloc(n, sizeof(int));
    begin = clock();
    srand(time(NULL));
    for (i = 0; i < n; i++) {
        t1[i] = rand() % 150;
        t2[i] = rand() % 150;
    }
    end = clock();

    printf("%f seconds to generate random values.\n\n", (double)(end - begin) / CLOCKS_PER_SEC);

    begin = clock();
    johnson_min_time_algorithm(n, t1, t2, &time_johnson);
    printf("\nShortest time : %d\n", time_johnson);
    end = clock();

    printf("\n%f seconds to determine shortest time for tasks.\n", (double)(end - begin) / CLOCKS_PER_SEC);

    return 0;
}
