#include <stdio.h>
#include <stdlib.h>
#include <time.h>

/* Funtion that implements Dijkstra's single source shortest path algorithm
   for a graph represented using adjacency matrix representation */
void dijkstra(int n, int** weights, int* shps, int src) {
    /* flagged[i] vaut 1 si la plus petite distance de la source au sommet i
       est trouvée ou bien que le sommet i est dans l'arbre des plus petits
       chemins
       Par défaut, flagged[i] vaut 0 */
    int* flagged = calloc(n, sizeof(int));
    int u = src;
    int i, v, min;

    /* Initialisation des distances à une valeur infinie */
    for (i = 0; i < n; i++)
        shps[i] = weights[src][i];
    shps[src] = 0; /* La distance de la source à elle même est toujours nulle */

    /* Recherche de la distance minimale à la source pour chaque sommet */
    for (i = 0; i <= n; i++) {
        min = RAND_MAX;

        /* On récupère le sommet de distance minimale parmi tous les sommets qui
           n'ont encore pas été traités (ie. flagged[v] est nul)
           Au cours de la première itération, le sommet recherché est toujours
           le sommet source */
        for (v = 0; v < n; v++) {
            if (flagged[v] == 0 && shps[v] != -1 && shps[v] <= min) {
                min = shps[v];
                u = v;
            }
        }

        flagged[u] = 1; /* On marque le sommet comme traité */

        /* Mise à jour du vecteur des distances minimales en mettant à jour la
           distance entre le somme choisi et ses voisins
           On ne met à jour shps[v] que si le sommet voisin n'est pas encore
           traité, qu'il existe bien un arc du sommet choisi au voisin et que
           le poids total de la source à v en passant par le sommet choisi
           est plus petite que la valeur actuelle de shps[v] */
        for (v = 0; v < n; v++)
            if (!flagged[v] && (weights[u][v] * shps[u] > 0))
                if ((shps[v] > weights[u][v] + shps[u]) || (shps[v] == -1))
                    shps[v] = weights[u][v] + shps[u];
    }
}

void dag_shortest_path_problem_from_source_algorithm(int n, int** weights, int* shps) {
    int* flagged = calloc(n, sizeof(int));
    int curr_vertex = 0;
    int i, v, min;

    for (i = 0; i < n; i++)
        shps[i] = weights[0][i];

    for (i = 0; i < n; i++) {
        min = RAND_MAX;

        for (v = 0; v < n; v++)
            if (flagged[v] == 0 && shps[v] != -1 && shps[v] <= min)
                min = shps[v], curr_vertex = v;

        flagged[curr_vertex] = 1;

        for (v = 0; v < n; v++)
            if (!flagged[v] && (weights[curr_vertex][v] * shps[curr_vertex] > 0))
                if ( (shps[v] > weights[curr_vertex][v] + shps[curr_vertex]) || (shps[v] == -1) )
                    shps[v] = weights[curr_vertex][v] + shps[curr_vertex];
    }
}


int main() {
    int i;
    int n = 5;

    int** weights = (int**) calloc(n, sizeof(int*));
    for (i = 0; i < n; i++)
        weights[i] = (int*) calloc(n, sizeof(int));

    int* shps = (int*) calloc(n, sizeof(int));

    /*weights[0][0] =  0; weights[1][0] = -1; weights[2][0] = -1; weights[3][0] = -1; weights[4][0] = -1; weights[5][0] = -1;
    weights[0][1] = -1; weights[1][1] =  0; weights[2][1] = -1; weights[3][1] = -1; weights[4][1] =  2; weights[5][1] =  3;
    weights[0][2] =  2; weights[1][2] =  6; weights[2][2] =  0; weights[3][2] = -1; weights[4][2] =  3; weights[5][2] = -1;
    weights[0][3] =  6; weights[1][3] = -1; weights[2][3] =  1; weights[3][3] =  0; weights[4][3] = -1; weights[5][3] = -1;
    weights[0][4] = -1; weights[1][4] =  3; weights[2][4] = -1; weights[3][4] =  6; weights[4][4] =  0; weights[5][4] =  1;
    weights[0][5] = -1; weights[1][5] = -1; weights[2][5] = -1; weights[3][5] =  1; weights[4][5] =  4; weights[5][5] =  0;*/

    clock_t begin, end;
    int j;

    /* Génération aléatoire d'un graphe de taille n*n */
    begin = clock();
    srand(time(NULL));
    for (i = 0; i < n; i++)
        for (j = 0; j < n; j++)
            weights[i][j] = (rand() % 10 <= 5) ? rand() % 10 : -1;
    for (i = 0; i < n; i++)
        weights[i][i] = 0;
    end = clock();

    for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++)
            printf("%5d", weights[i][j]);
        printf("\n");
    }

    printf("Elapsed: %f seconds\n", (double)(end - begin) / CLOCKS_PER_SEC);

    begin = clock();
    dag_shortest_path_problem_from_source_algorithm(n, weights, shps);
    end = clock();

    printf("Elapsed: %f seconds\n", (double)(end - begin) / CLOCKS_PER_SEC);

    for (i = 0; i < n; i++)
        printf("Distance from source to %d : %8d\n", i, shps[i]);

    return 0;
}
